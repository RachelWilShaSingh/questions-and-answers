# Pathing example, simple pygame program / Rachel Singh

import pygame, sys
from pygame.locals import *

class Action:
	def __init__( self ):
		self.xChange = 0
		self.yChange = 0
		self.countdown = 0
		
	def Setup( self, x, y, count ):
		self.xChange = x
		self.yChange = y
		self.countdown = count
		
	def Display( self ):
		print( self.xChange, self.yChange, self.countdown )

class Character:
	def __init__( self ):
		self.x = 1280/2
		self.y = 720/2
		self.surface = pygame.image.load( "bug.png" )
		
		self.actionList = []
		action1 = Action()
		action1.Setup( 0, -1, 100 )
		
		action2 = Action()
		action2.Setup( -1, 0, 200 )
		
		action3 = Action()
		action3.Setup( 1, 0, 400 )
		
		self.actionList.append( action1 )
		self.actionList.append( action2 )
		self.actionList.append( action3 )
	
	def Update( self ):
		
		
		# Actions are still in the action list
		if ( len( self.actionList ) > 0 ):	
						
			if ( self.actionList[0].countdown == 0 ):
				# Action is finished
				self.actionList.pop( 0 )
			else:
				# Do this action
				self.x += self.actionList[0].xChange
				self.y += self.actionList[0].yChange
				self.actionList[0].countdown -= 1
		
	def Draw( self, window ):
		window.blit( self.surface, ( self.x, self.y ) )
		
		
class Game:
	def __init__( self ):
		pygame.init()
		
		self.done = False
		self.fps = 60
		self.clock = pygame.time.Clock()
		self.window = pygame.display.set_mode( ( 1280, 720 ) )
		self.bgcolor = pygame.Color( 132, 200, 255 )
		pygame.display.set_caption( "Pathing example" )
		
		# Game character
		self.npc = Character()
		
	def Run( self ):
		while ( self.done == False ):
			# Events
			for event in pygame.event.get():
				if ( event.type == QUIT ):
					self.done = True
					
			# Updates
			self.npc.Update()
			
			# Drawing
			self.window.fill( self.bgcolor )
			self.npc.Draw( self.window )
			
			# Update screen
			pygame.display.update()
			self.clock.tick( self.fps )
		
		self.Cleanup()

	def Cleanup( self ):
		pygame.quit()
		sys.exit()


game = Game()
game.Run()
